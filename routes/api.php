<?php

use App\Http\Controllers\CalculateController;
use App\Http\Controllers\ExampleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route example api for success
Route::get('/example', [ExampleController::class, 'success']);

// Route example api for error
// Route::get('/example', [ExampleController::class, 'error']);

// Route calculate api
Route::get('/calculate', [CalculateController::class, 'execute']);
