#!/bin/sh

set -e

if [ -z "$1" ]
  then
    echo "No version supplied"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No message supplied"
    exit 1
fi

version="$1"
message="$2"

git push origin
git tag -a ${version} -m "${message}"
git push origin ${version}
