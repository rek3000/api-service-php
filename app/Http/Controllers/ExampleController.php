<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Application;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ExampleController
 * @package App\Http\Controllers
 */
class ExampleController extends Controller
{
    /**
     * @var bool
     */
    public bool $success;

    /**
     * @var int
     */
    public int $code;

    /**
     * @param bool $success
     * @param int $code
     */
    public function __construct(
        bool $success = true,
        int $code = 200
    ) {
        $this->success = $success;
        $this->code = $code;
    }

    public function success(): Application|Response|\Illuminate\Contracts\Foundation\Application|ResponseFactory
    {
        return response([
            'success' => $this->success,
            'code' => $this->code,
        ], $this->code);
    }

    public function error(): Application|Response|\Illuminate\Contracts\Foundation\Application|ResponseFactory
    {
        return response([
            'success' => false,
            'code' => 400,
        ], Response::HTTP_BAD_REQUEST);
    }
}
